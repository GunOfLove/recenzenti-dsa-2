#include <fstream>
#include <iostream>

using namespace std;

/** *classes*
 *  ░█████╗░██╗░░░░░░█████╗░░██████╗░██████╗███████╗░██████╗
 *  ██╔══██╗██║░░░░░██╔══██╗██╔════╝██╔════╝██╔════╝██╔════╝
 *  ██║░░╚═╝██║░░░░░███████║╚█████╗░╚█████╗░█████╗░░╚█████╗░
 *  ██║░░██╗██║░░░░░██╔══██║░╚═══██╗░╚═══██╗██╔══╝░░░╚═══██╗
 *  ╚█████╔╝███████╗██║░░██║██████╔╝██████╔╝███████╗██████╔╝
 *  ░╚════╝░╚══════╝╚═╝░░╚═╝╚═════╝░╚═════╝░╚══════╝╚═════╝░
*/

class Buffer {
    private:
        char* bufferArray;
        int bufferPosition = 0;

    public: 
        int bufferLength;
        Buffer(int length) {
            bufferLength = length;
            bufferArray = new char[length];
        }

        ~Buffer() {
            delete[] bufferArray;
        }

        char* get() {
            return bufferArray;
        }

        unsigned int getAsUnsignedInt() {
            return strtoul(bufferArray, NULL, 0);
        }

        int getAsInt() {
            return stoi(bufferArray, NULL, 0);
        }

        void readChar(char currentChar){
            if (bufferPosition >= bufferLength){
                clear();
            }
            bufferArray[bufferPosition] = currentChar;
            bufferPosition++;
        }

        void clear(){
            for (int i = 0; i < bufferLength; i++) {
                if (bufferArray[i] == '\0')
                    break;
                bufferArray[i] = '\0';
            }
            bufferPosition = 0;
        }
};

class Name{
    public:
        char* name;
        int keyCount;
        Name* next;
        int keyArray[60];
        Name(char* newName){
            name = newName;
            next = NULL;
            keyCount = 0;
        };

        ~Name(){
            delete[] name;
        }

        void addKey(int key){
            keyArray[keyCount] = key;
            keyCount++;
        }

        // Name(){
        //     next = NULL;
        // };
};


class OWLList {
    protected:
            Name *first;
            Name *last;
    public:
        Name *current;

        OWLList(){ //Kontrukstors ar value lauka aizpildi
            first = NULL;
            last = NULL;
            current = NULL;
        }

        ~OWLList();

        Name* addNext(char* val) {
            Name *node = new Name(val);
            if (first == NULL) {
                first = last = node;
            } else {
                last->next = node;
                last = node;
            }
            return node;
        } //Pievieno jaunu vērtību listes beigās un izmaina norādi uz pēdējo elementu.

        bool isEmpty() {
            return (first == NULL);
        }


        void start() {
            current = first;
        }
        bool end() {
            return (current == NULL);
        }

        void next() {
            if (!end()) {
                current = current->next;
            }
        }

        bool isInList(char* name){
            for (start(); !end(); next()) {
                for (int i = 0; name[i] != NULL; i++)
                {
                    if (current->name[i] != name[i]){
                        break;
                    }
                    return true;
                }
            }
            return false;
        }

        Name* getCurrent(){
            return current;
        }
};


class NameTable {
    public:
        OWLList* table;
        int tableLength;
        NameTable(int lenth) {
            tableLength = lenth;
            table = new OWLList[tableLength];
        }

        /**
         * @param Name to be inserted
         * @return Existing on new Name object, residing it it's "hash" position 
        */
        Name* insert(char* name) {
            int position = namePositionInTable(name);
            if (table[position].isInList(name)){
                return table[position].getCurrent();
            }
            return table[position].addNext(name);
        }

        int namePositionInTable(char* name) {
            int i=0;
            int sum=0;
            while (name[i] != NULL){
                sum += name[i];
                i++;
            }
            return sum % tableLength;
        }
};

class SBBSTNode
{
    public:
        int height, data;
        Name* owner;
        SBBSTNode *left, *right;
         /* Constructor */
        SBBSTNode()
        {
            left = NULL;
            right = NULL;
            data = 0;
            height = 0;
            owner = NULL;
        }

        /* Constructor */
        SBBSTNode(int n, Name* newOwner)
        {
            left = NULL;
            right = NULL;
            data = n;
            height = 0;
            owner = newOwner;
        }
};

/*
 * Stiched together from referecnces for base: https://www.sanfoundry.com/cpp-program-implement-self-balancing-binary-tree/
 * Stiched together from referecnces for delete: https://www.geeksforgeeks.org/avl-tree-set-2-deletion/
 * Class SelfBalancingBinarySearchTree
 */

class SelfBalancingBinarySearchTree
{
    private:
        SBBSTNode *root;
    public:
        bool nothingDeleted = false;
        bool nothingInserted = false;
        Name* foundName = NULL;
        SBBSTNode* deletedNode;
         /* Constructor */
        SelfBalancingBinarySearchTree()
        {
            root = NULL;
        }

         /* Function to check if tree is empty */
        bool isEmpty()
        {
            return root == NULL;
        }

         /* Make the tree logically empty */
        void makeEmpty()
        {
            root = NULL;
        }

         /* Function to insert data */
        void insert(int data, Name* owner)
        {
            root = insert(data, root, owner);
        }

         /* Function to delete data */
        void removeNode(int data)
        {
            root = deleteNode(root, data);
        }

         /* Function to get height of node */
         int height(SBBSTNode *t)
        {
            return t == NULL ? -1 : t->height;
        }

         /* Function to max of left/right node */
        int max(int lhs, int rhs)
        {
            return lhs > rhs ? lhs : rhs;
        }

         /* Function to insert data recursively */
         SBBSTNode *insert(int x, SBBSTNode *t, Name* owner)
        {
            nothingInserted = false;
            if (t == NULL)
                t = new SBBSTNode(x, owner);
            else if (x < t->data)
            {
                t->left = insert(x, t->left, owner);
                if (height(t->left) - height(t->right) == 2)
                    if (x < t->left->data)
                        t = rotateWithLeftChild(t);
                    else
                        t = doubleWithLeftChild(t);
            }
            else if (x > t->data)
            {
                t->right = insert(x, t->right, owner);
                if (height(t->right) - height(t->left) == 2)
                    if (x > t->right->data)
                        t = rotateWithRightChild(t);
                    else
                        t = doubleWithRightChild(t);
            } else {
                nothingInserted = true;
            }
            t->height = max(height(t->left), height(t->right)) + 1;
            return t;
        }

         /* Rotate binary tree node with left child */
         SBBSTNode *rotateWithLeftChild(SBBSTNode* k2)
        {
             SBBSTNode *k1 = k2->left;
            k2->left = k1->right;
            k1->right = k2;
            k2->height = max(height(k2->left), height(k2->right)) + 1;
            k1->height = max(height(k1->left), k2->height) + 1;
            return k1;
        }

         /* Rotate binary tree node with right child */
         SBBSTNode *rotateWithRightChild(SBBSTNode *k1)
        {
            SBBSTNode *k2 = k1->right;
            k1->right = k2->left;
            k2->left = k1;
            k1->height = max(height(k1->left), height(k1->right)) + 1;
            k2->height = max(height(k2->right), k1->height) + 1;
            return k2;
        }

        /*
          * Double rotate binary tree node: first left child
          * with its right child; then node k3 with new left child
          */
         SBBSTNode *doubleWithLeftChild(SBBSTNode *k3)
        {
            k3->left = rotateWithRightChild(k3->left);
            return rotateWithLeftChild(k3);
        }

        /*
          * Double rotate binary tree node: first right child
          * with its left child; then node k1 with new right child
          */
         SBBSTNode *doubleWithRightChild(SBBSTNode *k1)
        {
            k1->right = rotateWithLeftChild(k1->right);
            return rotateWithRightChild(k1);
        }

         /* Functions to count number of nodes */
        int countNodes()
        {
            return countNodes(root);
        }

         int countNodes(SBBSTNode *r)
        {
            if (r == NULL)
                return 0;
            else
            {
                int l = 1;
                l += countNodes(r->left);
                l += countNodes(r->right);
                return l;
            }
        }

         /* Functions to search for an element */
        bool search(int val)
        {
            return search(root, val);
        }

         bool search(SBBSTNode *r, int val)
        {
            bool found = false;
            while ((r != NULL) && !found)
            {
                int rval = r->data;
                if (val < rval)
                    r = r->left;
                else if (val > rval)
                    r = r->right;
                else
                {
                    found = true;
                    foundName = r->owner;
                    break;
                }
                found = search(r, val);
            }
            return found;
        }

         /* Function for inorder traversal */
        void inorder()
        {
            inorder(root);
        }

         void inorder(SBBSTNode *r)
        {
            if (r != NULL)
            {
                inorder(r->left);
                cout<<r->data<<"  ";
                inorder(r->right);
            }
        }

         /* Function for preorder traversal */
        void preorder()
        {
            preorder(root);
        }
         void preorder(SBBSTNode *r)
        {
            if (r != NULL)
            {
                cout<<r->data<<"  ";
                preorder(r->left);
                preorder(r->right);
            }
        }

         /* Function for postorder traversal */
        void postorder()
        {
            postorder(root);
        }
         void postorder(SBBSTNode *r)
        {
            if (r != NULL)
            {
                postorder(r->left);
                postorder(r->right);
                cout<<r->data<<"  ";
            }
        }

    SBBSTNode* deleteNode(SBBSTNode* root, int key) 
    { 
        nothingDeleted = false;
        // STEP 1: PERFORM STANDARD BST DELETE 
        if (root == NULL){
            nothingDeleted = true;
            return root;
        }; 
    
        // If the key to be deleted is smaller 
        // than the root's key, then it lies
        // in left subtree 
        if ( key < root->data ) 
            root->left = deleteNode(root->left, key); 
    
        // If the key to be deleted is greater 
        // than the root's key, then it lies 
        // in right subtree 
        else if( key > root->data ) 
            root->right = deleteNode(root->right, key); 
    
        // if key is same as root's key, then 
        // This is the node to be deleted 
        else
        { 
            // node with only one child or no child 
            if( (root->left == NULL) ||
                (root->right == NULL) ) 
            { 
                SBBSTNode *temp = root->left ? 
                            root->left : 
                            root->right; 
    
                // No child case 
                if (temp == NULL) 
                { 
                    temp = root; 
                    root = NULL; 
                } 
                else // One child case 
                *root = *temp; // Copy the contents of 
                            // the non-empty child 
                free(temp); 
            } 
            else
            { 
                // node with two children: Get the inorder 
                // successor (smallest in the right subtree) 
                SBBSTNode* temp = minValueNode(root->right); 
    
                // Copy the inorder successor's 
                // data to this node 
                root->data = temp->data; 
    
                // Delete the inorder successor 
                root->right = deleteNode(root->right, 
                                        temp->data); 
            } 
        } 
    
        // If the tree had only one node
        // then return 
        if (root == NULL) 
        return root; 
    
        // STEP 2: UPDATE HEIGHT OF THE CURRENT NODE 
        root->height = 1 + max(height(root->left), 
                            height(root->right)); 
    
        // STEP 3: GET THE BALANCE FACTOR OF 
        // THIS NODE (to check whether this 
        // node became unbalanced) 
        int balance = getBalance(root); 

        // If this node becomes unbalanced, 
        // then there are 4 cases 

        // Left Left Case 
        if (balance > 1 && 
            getBalance(root->left) >= 0) 
            return rightRotate(root); 

        // Left Right Case 
        if (balance > 1 && 
            getBalance(root->left) < 0) 
        { 
            root->left = leftRotate(root->left); 
            return rightRotate(root); 
        } 

        // Right Right Case 
        if (balance < -1 && 
            getBalance(root->right) <= 0) 
            return leftRotate(root); 

        // Right Left Case 
        if (balance < -1 && 
            getBalance(root->right) > 0) 
        { 
            root->right = rightRotate(root->right); 
            return leftRotate(root); 
        } 
    
        return root; 
    } 

    // Get Balance factor of node N 
    int getBalance(SBBSTNode *N) 
    { 
        if (N == NULL) 
            return 0; 
        return height(N->left) - 
            height(N->right); 
    } 

    /* Given a non-empty binary search tree, 
    return the node with minimum key value 
    found in that tree. Note that the entire 
    tree does not need to be searched. */
    SBBSTNode * minValueNode(SBBSTNode* node) 
    { 
        SBBSTNode* current = node; 
    
        /* loop down to find the leftmost leaf */
        while (current->left != NULL) 
            current = current->left; 
    
        return current; 
    }

    // A utility function to right
    // rotate subtree rooted with y 
    // See the diagram given above. 
    SBBSTNode *rightRotate(SBBSTNode *y) 
    { 
        SBBSTNode *x = y->left; 
        SBBSTNode *T2 = x->right; 
    
        // Perform rotation 
        x->right = y; 
        y->left = T2; 
    
        // Update heights 
        y->height = max(height(y->left), 
                        height(y->right)) + 1; 
        x->height = max(height(x->left), 
                        height(x->right)) + 1; 
    
        // Return new root 
        return x; 
    } 
    
    // A utility function to left 
    // rotate subtree rooted with x 
    // See the diagram given above. 
    SBBSTNode *leftRotate(SBBSTNode *x) 
    { 
        SBBSTNode *y = x->right; 
        SBBSTNode *T2 = y->left; 
    
        // Perform rotation 
        y->left = x; 
        x->right = T2; 
    
        // Update heights 
        x->height = max(height(x->left), 
                        height(x->right)) + 1; 
        y->height = max(height(y->left), 
                        height(y->right)) + 1; 
    
        // Return new root 
        return y; 
    }
};

/** *helpers*
 *  ██╗░░██╗███████╗██╗░░░░░██████╗░███████╗██████╗░░██████╗
 *  ██║░░██║██╔════╝██║░░░░░██╔══██╗██╔════╝██╔══██╗██╔════╝
 *  ███████║█████╗░░██║░░░░░██████╔╝█████╗░░██████╔╝╚█████╗░
 *  ██╔══██║██╔══╝░░██║░░░░░██╔═══╝░██╔══╝░░██╔══██╗░╚═══██╗
 *  ██║░░██║███████╗███████╗██║░░░░░███████╗██║░░██║██████╔╝
 *  ╚═╝░░╚═╝╚══════╝╚══════╝╚═╝░░░░░╚══════╝╚═╝░░╚═╝╚═════╝░
*/

// Taken from post.cpp
bool isLetter(char inputChar){
    if ((inputChar >= 65 && inputChar <= 90) ||
        (inputChar >= 97 && inputChar <= 122)) {
        return true;
    }
    return false;
}

bool isNumber(char inputChar){
    if ((inputChar >= 48 && inputChar <= 57)) {
        return true;
    }
    return false;
}

int checkFirstLetter(char inputSymbol) {
    if (inputSymbol==0){
        return -13;
    }
    if (!isNumber(inputSymbol)){
        return -13;
    }
    return 0;
}

bool compareString(char* stringOne, char* stringTwo){
    int i = 0;
    while (stringOne[i] != '\n') {
        if (stringOne[i] != stringTwo[i]){
            return false;
        }
        i++;
    }
    return true;
}

void eatWhitespace(fstream* fin, char* currentSymbol){
    while(*currentSymbol == ' ') {
        fin->get(*currentSymbol);
    };
}

char* readName(fstream* fin,char* currentSymbol){
    eatWhitespace(fin, currentSymbol);
    char* name = new char[101];
    int nameCharPosition = 0;
    while (*currentSymbol != ' '){
        name[nameCharPosition] = *currentSymbol;
        fin->get(*currentSymbol);
        nameCharPosition++;
    }
    name[nameCharPosition] = '\0';
    return name;
}

int readInt(fstream* fin,char* currentSymbol, Buffer* buffer){
    buffer->clear();
    eatWhitespace(fin, currentSymbol);
    while (isNumber(*currentSymbol)){
        buffer->readChar(*currentSymbol);
        fin->get(*currentSymbol);
    }
    return buffer->getAsInt();
}

bool sameString(char* stringOne, char* stringTwo){
    int finalCheck;
    for (int i = 0; stringOne[i] != NULL; i++)
    {
        if (stringOne[i] != stringTwo[i]){
            return false;
        }
        finalCheck = i;
    }
    if (stringOne[finalCheck+1] != stringTwo[finalCheck+1]){
        return false;
    }
    return true;
}

/** *main*
 *  ███╗░░░███╗░█████╗░██╗███╗░░██╗
 *  ████╗░████║██╔══██╗██║████╗░██║
 *  ██╔████╔██║███████║██║██╔██╗██║
 *  ██║╚██╔╝██║██╔══██║██║██║╚████║
 *  ██║░╚═╝░██║██║░░██║██║██║░╚███║
 *  ╚═╝░░░░░╚═╝╚═╝░░╚═╝╚═╝╚═╝░░╚══╝
*/

int main(){
    char fakeName[16] = {'d','o','l','b','e','n','g','e','l','\0'};
    Name* dummyName = new Name(fakeName);
    // File
    Buffer* readBuffer = new Buffer(32);
    char currentSymbol;
    char previousSymbol;
    char* currentName;
    int currentKeyCount;
    int keyArray[50];
    int keyToRemove;
    int keyToLookup;
    int intToPush;
    SelfBalancingBinarySearchTree pseudonymTree;
    NameTable nameTable(100);
    Name* currentNameObject;
    Name* nameToDelete;

    pseudonymTree.foundName = dummyName;

    bool hasDuplicateKeys = false;

        // fstream fin ("./test/in/reviewers.i9", ios::in);
        // fstream fout ("./test/out/reviewers.out", ios::out);

        fstream fin ("reviewers.in", ios::in);
        fstream fout ("reviewers.out", ios::out);

    fin.get(currentSymbol);

    if (currentSymbol != 'I'){
        fout << "nothing";
        return 0;
    }
    previousSymbol = currentSymbol;

    while (fin.get(currentSymbol)){
        int j = 0;
        switch (previousSymbol)
        {
        case 'I':
            hasDuplicateKeys = false;
            currentName = readName(&fin, &currentSymbol);
            currentKeyCount = readInt(&fin, &currentSymbol, readBuffer);
            for (int i = 0; i < currentKeyCount; i++){
                intToPush = readInt(&fin, &currentSymbol, readBuffer);
                if (pseudonymTree.search(intToPush)) {
                    if (sameString(pseudonymTree.foundName->name,currentName)){
                        continue;
                    } else {
                        fout << "no\n";
                        hasDuplicateKeys = true;
                        break;
                    }
                }
                keyArray[j] = intToPush;
                j++;
            }
            currentKeyCount = j;
            if (hasDuplicateKeys) break; // breaks insert cause of duplicate keys found
            sameString(pseudonymTree.foundName->name,currentName) 
            ? currentNameObject = pseudonymTree.foundName 
            : currentNameObject = nameTable.insert(currentName);
            for (int i = 0; i < currentKeyCount; i++){
                if (currentNameObject->keyCount == 50) {
                        fout << "no\n";
                        hasDuplicateKeys = true;
                        break;
                }
                pseudonymTree.insert(keyArray[i], currentNameObject);
                currentNameObject->addKey(keyArray[i]);
            }
            if (hasDuplicateKeys) break; // breaks insert cause of duplicate keys found
            fout << "ok\n";
            break;
        
        case 'D':
            keyToRemove = readInt(&fin, &currentSymbol, readBuffer);
            pseudonymTree.removeNode(keyToRemove);
            if (pseudonymTree.nothingDeleted) {
                fout << "no\n";
            } else {
                pseudonymTree.search(keyToRemove);
                nameToDelete = pseudonymTree.foundName;
                for (int i = 0; i < nameToDelete->keyCount; i++)
                {
                    pseudonymTree.removeNode(nameToDelete->keyArray[i]);
                }
                delete nameToDelete;
                fout << "ok\n";
                pseudonymTree.foundName = dummyName;
            }
            break;
        
        case 'L':
            keyToLookup = readInt(&fin, &currentSymbol, readBuffer);
            pseudonymTree.search(keyToLookup)
            ? fout << pseudonymTree.foundName->name << endl 
            : fout << "no\n";
            break;
        default:
            break;
        }
        previousSymbol = currentSymbol;
    }

    return 0;

}